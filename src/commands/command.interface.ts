import { AbstractCommand } from "./abstract-command";

export interface CommandInterface {
    new (message: any): AbstractCommand;
}
