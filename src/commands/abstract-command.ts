import { Message } from "discord.js";

export abstract class AbstractCommand {
    protected message: Message;

    constructor(message: Message) {
        this.message = message;
    }

    abstract parse(): void;
    abstract process(): Promise<unknown>;
}
