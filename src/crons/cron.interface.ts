import { AbstractCron } from "./abstract-cron";

export interface CronInterface {
    new (): AbstractCron;
}
