import { CronInterface } from "./cron.interface";

export abstract class AbstractCron {
    protected frequency: string;

    abstract run(): void;

    getFrequency(): string {
        return this.frequency;
    }
}
