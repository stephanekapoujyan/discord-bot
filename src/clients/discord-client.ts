import { Client, TextChannel } from 'discord.js';
import { AbstractCommand } from "../commands/abstract-command";
import { scheduleJob } from 'node-schedule';
import { CronInterface } from "../crons/cron.interface";
import { CommandInterface } from "../commands/command.interface";

export class DiscordClient {
    private static instance: DiscordClient;

    private client: Client;
    private readonly prefixCommand;
    private token: string;

    private commands: Map<string, CommandInterface>;
    private crons: Map<string, CronInterface>;

    static getInstance(token?: string): DiscordClient {
        if (!DiscordClient.instance) {
            DiscordClient.instance = new DiscordClient(token);
        }

        return DiscordClient.instance;
    }

    private constructor(token: string) {
        if (token === undefined) {
            throw new Error('discord bot token was not provided');
        }

        this.prefixCommand = '*';
        this.client = new Client();
        this.token = token;
        this.client.login(this.token);

        this.commands = new Map<string, CommandInterface>();
        this.crons = new Map<string, CronInterface>();
    }

    async init(): Promise<void> {
        return new Promise((resolve, reject) => {
            this.client.on('ready', () => {
                console.log('Ready');
                resolve();
            });
        })
    }

    handleMessage() {
        this.client.on('message', message => {
            const content = message.content;
            if (!content || !content.length || content.substr(0, 1) !== this.prefixCommand) {
                return;
            }

            let commandName = content.split(' ')[0];
            commandName = commandName.slice(1, commandName.length);

            if (!this.commands.has(commandName)) {
                return;
            }

            let Command = this.commands.get(commandName);
            const commandObj: AbstractCommand = new Command(message);
            commandObj.parse();
            commandObj.process();
        })
    }

    registerCommand(name: string, commandClass: CommandInterface): void {
        this.commands.set(name, commandClass);
    }

    registerCron(name: string, cronClass: CronInterface) {
        this.crons.set(name, cronClass);
        const instance = new cronClass();
        scheduleJob(instance.getFrequency(), instance.run.bind(instance));
    }

    getClient(): Client {
        return this.client;
    }

    getChannel(channelId: string, guildId: string): TextChannel {
        return this.client.guilds.get(guildId).channels.get(channelId) as TextChannel;
    }

    getMember(memberId: string, guildId: string) {
        return this.client.guilds.get(guildId).members.get(memberId);
    }

    editMessage(id: string, content: string) {
        // this.getChannel('lala');
        // this.client.guilds.get('').mess
    }
}
